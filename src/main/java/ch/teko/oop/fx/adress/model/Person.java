package ch.teko.oop.fx.adress.model;

import javafx.beans.property.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class Person
{
  private final StringProperty firstName;
  private final StringProperty lastName;
  private final StringProperty street;
  private final StringProperty city;
  private final IntegerProperty postalCode;
  private final ObjectProperty<LocalDate> birthday;

  public Person() {
    this(null, null);
  }

  public Person(String firstName, String lastName) {
    this.firstName = new SimpleStringProperty(firstName);
    this.lastName = new SimpleStringProperty(lastName);

    this.street = new SimpleStringProperty("some street");
    this.postalCode = new SimpleIntegerProperty(1234);
    this.city = new SimpleStringProperty("some city");

    LocalDate datum = LocalDate.parse("15.4.1983", DateTimeFormatter.ofPattern("d.M.yyyy"));
    this.birthday = new SimpleObjectProperty<LocalDate>(datum);
  }

  public String getFirstName() {
    return firstName.get();
  }

  public StringProperty getFirstNameProperty() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName.set(firstName);
  }

  public String getLastName() {
    return lastName.get();
  }

  public StringProperty getLastNameProperty() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName.set(lastName);
  }

  public String getStreet() {
    return street.get();
  }

  public StringProperty getStreetProperty() {
    return street;
  }

  public void setStreet(String street) {
    this.street.set(street);
  }

  public int getPostalCode() {
    return postalCode.get();
  }

  public IntegerProperty getPostalCodeProperty() {
    return postalCode;
  }

  public void setPostalCode(int postalCode) {
    this.postalCode.set(postalCode);
  }

  public String getCity() {
    return city.get();
  }

  public StringProperty getCityProperty() {
    return city;
  }

  public void setCity(String city) {
    this.city.set(city);
  }

  public LocalDate getBirthday() {
    return this.birthday.get();
  }

  public void setBirthday(LocalDate birthday) {
    this.birthday.set(birthday);
  }

  public ObjectProperty<LocalDate> birthdayProperty() {
    return birthday;
  }

}